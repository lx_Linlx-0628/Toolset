package com.toolset;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import lombok.extern.slf4j.Slf4j;

/**
 * 启动程序
 *
 * @author toolset
 */
@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ToolsetApplication {
    public static void main(String[] args) {
        log.info("项目开始启动");
        SpringApplication.run(ToolsetApplication.class, args);
        log.info("项目启动成功");
    }
}