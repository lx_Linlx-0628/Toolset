package com.toolset.omnipotent.service;

import com.toolset.common.json.JSONObject;

public interface OcrService {

    /**
     * Tesseract OCR
     * @return
     */
    JSONObject tesseract();
}
