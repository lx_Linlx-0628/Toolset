package com.toolset.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.toolset.system.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author toolset
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}